from math import comb, factorial
from numpy import array, zeros, dot
from random import shuffle
from typing import Callable, Tuple, Dict

###### USER INPUT VARIABLES ###### 

# this variable indicates the maximum n for which Dp is computed
n_max: int = 50

# this variable contains the output filenames and the values of p
files: Tuple[Tuple[str,float], ...] = (('PUSH50', 0.5), ('PUSH95', 0.95), ('PUSH99', 0.99))

# this variable selects the model; it must contain either 'push' or 'pull'
model: str = 'push'

##################################

# validation of user input
if not all(map(lambda x:type(x[1]) == float and 0 < x[1] < 1, files)):
    raise ValueError('parameter \'p\' in variable \'files\' must be between 0 and 1')

# recursively computes the Stirling numbers of the second kind
def stirling2(n: int, k: int) -> int:
	key = str(n) + "," + str(k)

	if key in computed.keys():
		return computed[key]
	if n == k == 0:
		return 1
	if (n > 0 and k == 0) or (n == 0 and k > 0):
		return 0
	if n == k:
		return 1
	if k > n:
		return 0
	result = k * stirling2(n - 1, k) + stirling2(n - 1, k - 1)
	computed[key] = result
	return result

# computes P(X(t+1) = b | X(t) = a) in the PUSH model
def coef_push(n: int, a: int, b: int) -> float:
    if b > 2 * a:
        return 0.0
    som = 0
    for i in range(b - a, a + 1):
        som += comb(a, i) * comb(n - a, b - a) * stirling2(i, b - a) * a ** (a - i) * factorial(b - a)
    return som / n ** a

# computes P(X(t+1) = b | X(t) = a) in the PULL model
def coef_pull(n: int, a: int, b: int) -> float:
    return (comb(n - a, b - a) * (n - a) ** (n - b) * a ** (b - a)) / n ** (n - a)

# validation of user input
if model == 'push':
    f = coef_push
elif model == 'pull':
    f = coef_pull
else:
    raise ValueError('wrong \'model\' variable')

computed: Dict[str,int] = {} # cache for the Stirling numbers

def compute_matrix(n: int, f: Callable[[int, int, int],float]):
    m = zeros((n, n))
    for i in range(n):
        for j in range(i, n):
            m[i][j] = f(n, i + 1, j + 1)
    return m

cache_matrices = {} # cache for the matrices of probability P(X(t+1) = b | X(t) = a)

def compute(n: int , p: float, f: Callable[[int, int, int],float]) -> int:
    if not n in cache_matrices:
        print(f'computing transition matrix of size {n}')
        cache_matrices[n] = compute_matrix(n, f).transpose()
    matrix = cache_matrices[n]
    vec = zeros((n, 1))
    vec[0][0] = 1.0
    counter = 0

    # Given the matrix M containing the coefficients P(X(t+1) = b | X(t) = a)
    # and the vector V representing the initial state (only one node informed)
    # we compute Dp by finding the counter c such that the n-th coefficients of the vector M^c × V is greater than p.
    while vec[n-1][0] < p:
        counter += 1
        vec = matrix.dot(vec)
    return counter

# if Dp is the same for n = 3 and n = 7, then it is also the same for any n between 4 and 6
def deduce_from_bounds(Dp, i):
    s_low  = set( Dp[j] for j in range(1,i) if j in Dp )
    s_high = set( Dp[j] for j in range(i+1,n_max) if j in Dp )
    if len(s_high) == 0 or len(s_low) == 0 or max(s_low) != min(s_high):
        return None
    else:
        return max(s_low)

def write(name, Dp):
    with open(name, 'w') as fil:
        for i in sorted(list(Dp.keys())):
            fil.write(f'{i},{Dp[i]}\n')

for name, p in files:
    print(f'starting {name} series')
    Dp = dict()

    # create file
    with open(name, 'a') as fil:
        pass

    # retrieve Dp from existing file
    with open(name, 'r') as fil:
        for l in fil.readlines():
            if ',' in l:
                n,d = l[:-1].split(',')
                Dp[int(n)] = int(d)

    # plan the computation of Dp for each n in a random order,
    # to take advantage of the 'deduce_from_bounds' function and the 'matrixed' cache 
    Dp[1] = 0
    missing = list(i for i in range(1,n_max) if i not in Dp and not i in cache_matrices)
    shuffle(missing)
    missing = list(i for i in range(1,n_max) if i not in Dp and i in cache_matrices) + missing

    try:
        while len(missing) > 0:
            i, missing = missing[0], missing[1:]
            ded = deduce_from_bounds(Dp, i)
            if ded == None:
                Dp[i] = compute(i, p, f)
            else:
                Dp[i] = ded
    except KeyboardInterrupt:
        write(name, Dp)
        raise KeyboardInterrupt
    else:
        write(name, Dp)
