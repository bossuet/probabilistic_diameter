#!/bin/env python3

import csv
import matplotlib
from math import log
import matplotlib.pyplot as plt
import numpy as np
from typing import Tuple

###### USER INPUT VARIABLES ###### 

# If this variable is true, the chart is saved according to the 'file_name' variable
# otherwise, the chart is displayed.
pgf = False
file_name = 'Dcomplet.pgf'

# this variable contains the input filenames and the values of p
files: Tuple[Tuple[str,float], ...] = (('PUSH99', 0.99), ('PUSH95', 0.95), ('PUSH50', 0.5))

# this variable indicates the maximum n for which Dp is displayed
n_max: int = 150

# this function is plotted in the chart to represent the theoretical bound on Dp
curve = lambda x : 2 * log(x, 2) + log(x)

##################################

if pgf:
    matplotlib.use("pgf")
    matplotlib.rcParams.update({
        "pgf.texsystem": "pdflatex",
        'font.family': 'serif',
        'text.usetex': True,
        'pgf.rcfonts': False,
    })


for name, p in files:
    with open(name, 'r') as f:
        curs = csv.reader(f, delimiter = ',')
        series = tuple( (int(l[0]), int(l[1])) for l in curs if len(l) > 1 and int(l[0]) <= n_max ) 

    series = tuple(zip(*series))
    plt.semilogx(series[0], series[1], '.', label=f'$p = {p}$', markersize = 2)

plt.semilogx(range(2, n_max), list(map(curve , range(2, n_max))) )

plt.xlabel('$\log n$')
plt.ylabel('$\hat D^{(1)}(p)$')
plt.yticks(range(int(plt.subplot().get_ylim()[1])))
plt.legend()

if pgf:
    fig = plt.gcf()
    fig.set_size_inches(w=4.7747, h=3.5)
    fig.tight_layout()
    plt.savefig(file_name)
else:
    plt.show()
